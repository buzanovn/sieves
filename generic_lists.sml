fun ?->(n: ''a, []: ''a list) = false
  | ?-> (n: ''a, (h::t): ''a list) = (n = h) orelse if (t = nil) then false else ?->(n, t);
infix 2 ?->;

fun !->(n: ''a, lst: ''a list) = not(n ?-> lst);
infix 2 !->;

fun <->(n,m) = if (m < n) then nil else if (m = n) then [n] else [n] @ <->(n+1, m);
infix 2 <->;

fun work_with_duplicates ([], func: ''a * ''a list -> bool) = []
  | work_with_duplicates ((h::t), func) = (if (func(h, t)) then [h] else []) @ work_with_duplicates(t, func); 

(* Intersection like A |^| B *)
fun &&([]: ''a list, []: ''a list) = []
	| &&([], oth: ''a list) = []
	| &&(t: ''a list, []) = []
	| &&(t: ''a list, oth: ''a list) = work_with_duplicates(t @ oth, op ?->);

(* Union like A |_| B *)
fun ||([]: ''a list, []: ''a list) = []
	| ||([], oth: ''a list) = oth
	| ||(t: ''a list, []) = t
	| ||(t: ''a list, oth: ''a list) = work_with_duplicates(t @ oth, op !->);

infix 2 &&;
infix 2 ||;
	
fun ??->([]: char list, s: char) = 0
	| ??->((h::t) : char list, s : char) = (if (h = s) then 1 else 0) + (if not(t = nil) then ??->(t,s) else 0);
infix 2 ??->;

(*Throws out element s from list*)
fun << ([]: ''a list, s: ''a) = []
	| << ((h::t) : ''a list, s : ''a) = List.filter (fn x => x <> s) (h::t);
infix 2 <<;

(*Throws out elements from second list from first one*)
fun <<<<([]: ''a list, []: ''a list) = []
  | <<<<([], (h1::t1)) = []
  | <<<<((h::t), []) = (h::t)
  | <<<<((h::t), (h1::t1)) = <<<<((h::t) << h1, t1);

infix 2 <<<<;