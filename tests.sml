fun printTest(numtest: string, something) = print("Test " ^ numtest ^ " ---> " ^ something ^ "\n");

fun printGenericTest(numtest: string, func, some_generic_thing) = printTest(numtest, func(some_generic_thing));

fun printIntTest(numtest, func_ret_int) = printGenericTest(numtest, Int.toString, (func_ret_int));

fun printBooleanTest(numtest, func_ret_bool) = printGenericTest(numtest, (fn b => if(b) then "true" else "false"), func_ret_bool);

fun wrapListToString(func, lst) = let
	val str = func(lst);
in
	if (str = "empty list") then str else "[" ^ str ^ "]"
end;

fun intlist_to_string [] = "empty list"
    | intlist_to_string (h::t) = (Int.toString h) ^ (if not(t = nil) then "," ^ (intlist_to_string t) else "");

fun stringlist_to_string [] = "empty list"
	| stringlist_to_string (h::t) = h ^ (if not(t = nil) then "," ^ (stringlist_to_string t) else "");

fun printIntListTest(numtest, func_ret_int_list) = printTest(numtest, wrapListToString(intlist_to_string, func_ret_int_list));

fun printStringListTest(numtest, func_ret_str_list) = printTest(numtest, wrapListToString(stringlist_to_string, func_ret_str_list));

fun printStringTest(numtest, str: string) = print ("Test " ^ numtest ^ " ---> " ^ str ^ "\n");