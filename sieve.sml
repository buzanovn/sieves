use "generic_lists.sml";
use "tests.sml";

(* Filtering the list with the rule: each l of list is a <= l <= b*)
val filter_between = (fn (a,b,lst) => List.filter (fn(x) => (x <= b) andalso (x >= a)) lst);

fun pair(a: int, lst: int list) = List.map (fn (x) => (a, x)) lst;

val sundaram_number = (fn (x,y) => x + y + 2 * x * y);

fun map_by_sundaram([]) = []
  | map_by_sundaram(lst) = List.map (fn x => x * 2 + 1) lst;

(* Define signature (like abstract class or interface) of Sieve *)
signature Sieve = sig
    val sieve: (int * int) -> int list
end;

(* Define structure of Euratosphen's sieve  *)
structure EratosphenSieve :> Sieve = struct
(* According to Euratosphen's algorithm we define a simple filtering function *)
fun filter_by_mod(n, []) = []
  | filter_by_mod(n, lst)= List.filter (fn x => x = n orelse x mod n <> 0) lst;

fun sieve(from, to) = let
    val lst = 2 <-> to;
    fun loop_through [] = []
      | loop_through (h::t) = h::loop_through(filter_by_mod(h, t));
in
    filter_between(from, to, loop_through(lst))
end
end;

(* Define a naive structure of Sundaram. It's naive and could be optimized, because
we can define the interval's of generating pairs of (i,j) for each i for each j according to every i and j can be  *)
structure SundaramSieve :> Sieve = struct
  fun gen_cartesian(from, to) = let
      val i = 1 <-> to;
      val j = (fn (i) => i <-> to);
      fun apply([], func: int * int list -> (int * int) list) = []
        | apply(h::t,  func: int * int list -> (int * int) list) = func(h, j(h)) @ apply(t, func); 
  in
      List.filter (fn (i,j) => sundaram_number(i,j) <= to) (apply(i, pair))
  end;

  (*fun filter_by_sundaram([], i, j) = []
    | filter_by_sundaram(lst, i, j) = List.filter (fn x => sundaram_number(i,j)) lst;*)

  fun sieve(from, to) = let
      val bottom_border = Real.floor(((Real.fromInt(from)) / 2.0));
      val lst = bottom_border <-> to;
      val cartesian = gen_cartesian(from, to);
      val sundaram = List.map sundaram_number cartesian;
  in
      filter_between(from, to, map_by_sundaram (lst <<<< sundaram))
  end
end;

printIntListTest("EratosphenSieve test from 2 to 100", EratosphenSieve.sieve(2, 100));
printIntListTest("EratosphenSieve test from 1000 to 2000", EratosphenSieve.sieve(1000, 2000));

printIntListTest("SundaramSieve test from 2 to 100", SundaramSieve.sieve(2, 100));
printIntListTest("SundaramSieve test from 1000 to 2000", SundaramSieve.sieve(5000, 6000));